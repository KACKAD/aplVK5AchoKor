Да, у меня проблемы с компиляцией и сборкой под linux (возможно опыта не хватает,
а возможно технические), и именно поэтому нет Makefile (в котором скорее всего
 было бы "g++ main.cpp"),
потому программа собиралась и тестировалась в visual
 studio 2010. За основу взят алгоритм Ахо-Корасик,
ибо по описанию именно этим он и должен заниматься - поиск строки в словаре.
В программе строю бор, добавляя туда строки из словаря (добавление одной строки
происходит за О(L), где L - длина строки. Соответственно проверка строки на
принадлежность бору происходит так же за О от длины строки. Итого предполагая,
что длина строки небольшая, получаем время работы О(n), где n - общее число 
строк.