#include <stdio.h>
#include <string.h>
#include <ctype.h>

const int MAXASCII = 256;
struct node{
	bool leaf;
	node* parent;
	node* kids[MAXASCII];
};

typedef node* pnode;
pnode bor;
void InitNode(pnode p)
{
	for(int i = 0; i < MAXASCII; ++i)
	{
		p->kids[i] = NULL;
	}
	
	p->leaf = false;
	p->parent = NULL;
}

void Init()
{
	bor = new node();
	InitNode(bor);
}

void Put(char* s)
{
	pnode cur = bor;
	for(int i = 0; i < strlen(s); ++i)
	{
		if(cur->kids[(int)s[i]] == NULL)
		{
			cur->kids[(int)s[i]] = new node();
			InitNode(cur->kids[(int)s[i]]);
			cur->kids[(int)s[i]]->parent = cur;
		}
		cur = cur->kids[(int)s[i]];
	}
	cur->leaf = true;
}

bool Search(char* s)
{
	pnode cur = bor;
	
	for(int i = 0; i < strlen(s); ++i)
	{
		if(cur->kids[(int)s[i]] == NULL)
		{
			return false;
		}
		else
		{
			cur = cur->kids[(int)s[i]];
		}
	}
	return cur->leaf;
}

int main(int argc, char* argv[])
{
	Init();
	char* str = new char;
	
	if(argc == 2)
	{
		FILE * dict = new FILE;
		dict = fopen(argv[1], "r");
				
		while (fscanf(dict, "%s\n", str) != EOF)
		{
			
			Put(str);
		
		}
		fclose(dict);
	}
	strcpy(str, "string");
	while (strcmp(str, "exit") != 0)
	{
		scanf("%s\n", str);
		if(Search(str))
		{
			printf("YES\n");
		}
		else
		{
			printf("NO\n");
		}
	}
	return 0;
}